export class Movie {
    public title: string;
    public actors: string;
    public studio: string;
    public year: string;
    public genre_id: string;
    public image: string;

    constructor (title: string, actors: string, studio: string, year: string, genre_id: string, image: string) {
        this.title = title;
        this.actors = actors;
        this.studio = studio;
        this.year = year;
        this.genre_id = genre_id;
        this.image = image;
    }
}