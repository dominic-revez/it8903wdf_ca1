import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Movie } from "../movies.model";
import { MoviesService } from "../movies.service";

@Component({
  selector: 'app-movie-detail',
  templateUrl: './movie-detail.component.html',
  styleUrls: ['./movie-detail.component.css']
})
export class MovieDetailComponent implements OnInit {

  mid: string;
  myMovie: Movie [] = [];

  constructor(
    private route: ActivatedRoute,
    public moviesService: MoviesService
  ) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.mid = params.id
    });
    
    // console.log(this.mid);
    // console.log(this.moviesService.getMovieDetails(this.mid));

    this.myMovie[0] = this.moviesService.getMovieDetails(this.mid);
    console.log(this.myMovie[0]);  
    
  }
  
}
