import { Component, OnInit, Input } from '@angular/core';
import { Movie } from "../movies.model";
import { Genre } from "../genres.model";
import { MoviesService } from "../movies.service";

@Component({
  selector: 'app-movies-list',
  templateUrl: './movies-list.component.html',
  styleUrls: ['./movies-list.component.css']
})
export class MoviesListComponent implements OnInit {

  @Input() myMovies: Movie [] = [];
  genreList: Genre [] = [];

  constructor(public moviesService: MoviesService) { }

  ngOnInit() {
    // console.log(this.myMovies);
    this.genreList = this.moviesService.getGenres();
  }

  genreChange(elem: HTMLInputElement) {
    // console.log(this.moviesService.filterMoviesByGenre(elem.value));
    this.myMovies = this.moviesService.filterMoviesByGenre(elem.value);
  }
}
