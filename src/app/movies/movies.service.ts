import { Injectable } from '@angular/core';
import { Movie } from "./movies.model";
import { Genre } from "./genres.model";

@Injectable({
  providedIn: 'root'
})
export class MoviesService {

  movieList : Movie [] = [
    new Movie('Title 0', 'Actor 1, Actor 2', 'Studio', 'Year', '1', 'intro.jpg'),
    new Movie('Title 1', 'Actor 1, Actor 2', 'Studio', 'Year', '2', 'intro.jpg'),
    new Movie('Title 2', 'Actor 1, Actor 2', 'Studio', 'Year', '2', 'intro.jpg'),
    new Movie('Title 3', 'Actor 1, Actor 2', 'Studio', 'Year', '3', 'intro.jpg'),
    new Movie('Title 4', 'Actor 1, Actor 2', 'Studio', 'Year', '3', 'intro.jpg'),
    new Movie('Title 5', 'Actor 1, Actor 2', 'Studio', 'Year', '3', 'intro.jpg'),
    new Movie('Title 6', 'Actor 1, Actor 2', 'Studio', 'Year', '4', 'intro.jpg'),
    new Movie('Title 7', 'Actor 1, Actor 2', 'Studio', 'Year', '4', 'intro.jpg'),
    new Movie('Title 8', 'Actor 1, Actor 2', 'Studio', 'Year', '4', 'intro.jpg'),
    new Movie('Title 9', 'Actor 1, Actor 2', 'Studio', 'Year', '4', 'intro.jpg'),
    new Movie('Title 10', 'Actor 1, Actor 2', 'Studio', 'Year', '5', 'intro.jpg'),
    new Movie('Title 11', 'Actor 1, Actor 2', 'Studio', 'Year', '5', 'intro.jpg'),
    new Movie('Title 12', 'Actor 1, Actor 2', 'Studio', 'Year', '5', 'intro.jpg'),
    new Movie('Title 13', 'Actor 1, Actor 2', 'Studio', 'Year', '5', 'intro.jpg'),
    new Movie('Title 14', 'Actor 1, Actor 2', 'Studio', 'Year', '5', 'intro.jpg'),
  ]

  genreList : Genre [] = [
    new Genre('1', 'Genre 1', 'Description'),
    new Genre('2', 'Genre 2', 'Description'),
    new Genre('3', 'Genre 3', 'Description'),
    new Genre('4', 'Genre 4', 'Description'),
    new Genre('5', 'Genre 5', 'Description')
  ]

  constructor() { }

  getMovies () {
    return this.movieList.slice();
  }

  getGenres () {
    return this.genreList.slice();
  }

  filterMoviesByGenre (id) {
    var filtered = [];
    for (var i = 0; i < this.movieList.length; i++)
    {
      if (id == this.movieList[i].genre_id)
      {
        filtered[filtered.length] = this.movieList[i];
      }
    }
    return filtered;
  }

  getMovieDetails (id) {
    return this.movieList[id];
  }
}
