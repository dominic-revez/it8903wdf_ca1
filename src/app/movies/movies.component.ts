import { Component, OnInit } from '@angular/core';
import { Movie } from "./movies.model";
import { MoviesService } from "./movies.service";

@Component({
  selector: 'app-movies',
  templateUrl: './movies.component.html',
  styleUrls: ['./movies.component.css']
})
export class MoviesComponent implements OnInit {

  movieList: Movie [] = [];

  constructor(public moviesService: MoviesService) { }

  ngOnInit() {
    this.movieList = this.moviesService.getMovies();

  }

}
