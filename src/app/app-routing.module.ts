import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from "@angular/router";

import { HomeComponent } from "./home/home.component";
import { MoviesComponent } from "./movies/movies.component";
import { MovieDetailComponent } from "./movies/movie-detail/movie-detail.component";
import { FeedbackComponent } from "./feedback/feedback.component";
import { PageNotFoundComponent } from "./page-not-found/page-not-found.component";

const appRoutes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  { path: 'browse', component: MoviesComponent },
  { path: 'browse/movie/:id', component: MovieDetailComponent },
  { path: 'form', component: FeedbackComponent },
  { path: 'not-found', component: PageNotFoundComponent },
  { path: '**', redirectTo: '/not-found' }
]

@NgModule({
  declarations: [],
  imports: [
    CommonModule
    ,RouterModule.forRoot(appRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
