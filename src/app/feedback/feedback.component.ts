import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, ValidatorFn } from "@angular/forms";

@Component({
  selector: 'app-feedback',
  templateUrl: './feedback.component.html',
  styleUrls: ['./feedback.component.css']
})
export class FeedbackComponent implements OnInit {
  
  feedbackForm: FormGroup;

  constructor() { }

  ngOnInit() {
    this.feedbackForm = new FormGroup({
      email: new FormControl(null, [Validators.required, Validators.email]),
      subject: new FormControl(null, [Validators.required, this.blankSpaces]),
      feedback: new FormControl(null, Validators.required)
    });
  }

  onSubmit() {
    console.log("Form submitted");
    console.log(this.feedbackForm);
    console.log(this.feedbackForm.value);
  }

  blankSpaces(control: FormControl) : {[s: string] : boolean} {
    if (control.value != null && control.value.trim().length === 0) {
      return {blankSpaces: true};
    }
    return null;
  }

}
